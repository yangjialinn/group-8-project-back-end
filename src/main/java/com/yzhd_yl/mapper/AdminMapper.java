package com.yzhd_yl.mapper;

import com.yzhd_yl.entity.AdminQuery;
import com.yzhd_yl.entity.SysAdmin;
import com.yzhd_yl.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface AdminMapper {

    //根据用户id查询菜单
    List<SysMenu> findByAdminId(Long adminId);
    //查询总数
    Long findTotal(AdminQuery adminQuery);
    //分页查询数据
    List<SysAdmin> findData(AdminQuery userQuery);
    //保存用户
    void saveAdmin(SysAdmin sysAdmin);
    //删除数据deleteAdminById
    void deleteAdminById(Long id);
    //修改用户
    void updateAdmin(SysAdmin umsAdmin);
    //保存用户角色
    void saveAdminRole(List<Map> list);
    //deleteAdminRoleByAdminId
    void deleteAdminRoleByAdminId(Long userId);
    //根据用户名查询用户
    SysAdmin selectByAdminname(String username);

}
