package com.yzhd_yl.exception;

/**
 * @description: BaseException 异常处理
 */
public class BaseException extends RuntimeException {

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }
}

