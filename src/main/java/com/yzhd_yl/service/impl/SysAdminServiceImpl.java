package com.yzhd_yl.service.impl;

import com.yzhd_yl.entity.AdminQuery;
import com.yzhd_yl.entity.SysAdmin;
import com.yzhd_yl.entity.SysMenu;
import com.yzhd_yl.mapper.AdminMapper;
import com.yzhd_yl.service.ISysAdminService;
import com.yzhd_yl.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 用户管理服务
 * <p>
 * Description:
 * </p>
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class SysAdminServiceImpl implements ISysAdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Override
    public SysAdmin get(String adminname) {
        return adminMapper.selectByAdminname(adminname);
    }

    @Override
    public List<SysMenu> findByAdminId(Long userId) {
        return adminMapper.findByAdminId(userId);
    }

    @Override
    @Transactional
    public PageList findPage(AdminQuery adminQuery) {
        Long total =  adminMapper.findTotal(adminQuery);
        List<SysAdmin> rows = adminMapper.findData(adminQuery);
        PageList pageList = new PageList();
        pageList.setTotal(total);
        pageList.setRows(rows);
        return pageList;
    }

    //保存用户
    @Override
    @Transactional
    public void saveAdmin(SysAdmin sysAdmin) {
        sysAdmin.setPassword(passwordEncoder.encode(sysAdmin.getPassword()));
        sysAdmin.setStatus(1);
        sysAdmin.setCreateTime(new Date());
        adminMapper.saveAdmin(sysAdmin);
    }

    @Override
    public void deleteAdminById(Long id) {
        adminMapper.deleteAdminById(id);
    }

    @Override
    public void updateAdmin(SysAdmin sysAdmin) {
        sysAdmin.setPassword(passwordEncoder.encode(sysAdmin.getPassword()));
        adminMapper.updateAdmin(sysAdmin);
    }

    /**
     *  @description: 保存用户角色
     *  @params: {roleId:1,userId:1}
     *  @return
     */
    @Override
    public void saveAdminRole(List<Map> list) {
        Long adminId = 0L;
        for (Map map : list) {
            Integer adminIdStr = (Integer)map.get("userId");
            adminId = Long.valueOf(adminIdStr);
        }
        //先删除用户的角色
        adminMapper.deleteAdminRoleByAdminId(adminId);
        //在保存
        adminMapper.saveAdminRole(list);
    }

    /**
     * 初始化用户对象
     * @param sysAdmin
     */
    private void initSysAdmin(SysAdmin sysAdmin) {
        // 初始化创建时间
        sysAdmin.setCreateTime(new Date());
        sysAdmin.setLoginTime(new Date());

        // 初始化状态
        if (sysAdmin.getStatus() == null) {
            sysAdmin.setStatus(0);
        }
        // 密码加密
        sysAdmin.setPassword(passwordEncoder.encode(sysAdmin.getPassword()));
    }



}
