package com.yzhd_yl.service;

import com.yzhd_yl.entity.AdminQuery;
import com.yzhd_yl.entity.SysAdmin;
import com.yzhd_yl.entity.SysMenu;
import com.yzhd_yl.util.PageList;

import java.util.List;
import java.util.Map;

public interface ISysAdminService {
    /**
     *  @description:  根据用户名得到用户
     *  @params:  username
     *  @return  返回用户信息
     *
     */

    SysAdmin get(String username);

    /**
     *  @description: 根据用户id查询菜单
     *  @Params:   userId
     *  @Return   List<SysMenu>
     *
     */
    List<SysMenu> findByAdminId(Long userId);

    /**
     *  @description: 分页查询用户列表
     *  @Params:   userQuery
     *  @Return   PageList<SysUser>
     *
     */
    PageList findPage(AdminQuery adminQuery);


    /**
     *  @description: 保存用户
     *  @Params:   umsAdmin
     *  @Return   void
     *
     */
    void saveAdmin(SysAdmin sysAdmin);

    /**
     *  @description: 删除数据deleteUserById
     *  @Params:   id
     *  @Return   void
     *
     */
    void deleteAdminById(Long id);
    /**
     *  @description: 修改数据
     *  @Params:   sysUser
     *  @Return   void
     *
     */
    void updateAdmin(SysAdmin sysAdmin);
    /**
     *  @description: 保存用户角色
     *  @Params:   List<Map> mp
     *  @Return   void
     *
     */
    void saveAdminRole(List<Map> mp);
}
