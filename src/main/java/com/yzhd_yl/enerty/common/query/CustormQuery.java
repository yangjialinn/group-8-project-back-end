package com.yzhd_yl.enerty.common.query;

import lombok.Data;

/**
 * @description: CustormQuery 顾客查询条件
 * @author: soulcoder 灵魂码仔
 * @email: 2579692606@qq.com
 * @date: created by 2020/10/29
 * @copyright: itxfq 项目分享圈
 */
@Data
public class CustormQuery extends  BaseQuery {

    private String account;
    private String username;
    private String password;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CustormQuery(String account, String username, String password) {
        this.account = account;
        this.username = username;
        this.password = password;
    }

    public CustormQuery() {
    }
}
