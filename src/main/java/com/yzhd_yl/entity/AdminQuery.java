package com.yzhd_yl.entity;

import lombok.Data;

/**
 * @description: UserQuery 用户查询条件
 */
@Data
public class AdminQuery extends BaseQuery {

    //登录用户id
    private Integer id;

    private String username;

    private String email;



}
