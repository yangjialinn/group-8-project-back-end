package com.yzhd_yl.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class LoginAdmin implements Serializable {
    private String name;
    private String avatar;
    private String nickName;
    private List<SysMenu> menuList = new ArrayList();
}
