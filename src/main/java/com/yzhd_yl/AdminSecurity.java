package com.yzhd_yl;

import com.yzhd_yl.entity.SysAdmin;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Set;

//登录用户封装
public class AdminSecurity extends User {

    SysAdmin loginAdmin;
    private String token;

    public AdminSecurity(SysAdmin admin, Set<GrantedAuthority> authorities) {
        super(admin.getUsername(), admin.getPassword(),true,true,true,true, authorities);
        this.loginAdmin = admin;
    }

    public SysAdmin getLoginAdmin() {
        return loginAdmin;
    }

    public void setLoginAdmin(SysAdmin loginAdmin) {
        this.loginAdmin = loginAdmin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
