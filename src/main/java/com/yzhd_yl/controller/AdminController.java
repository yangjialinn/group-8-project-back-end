package com.yzhd_yl.controller;

import com.yzhd_yl.entity.*;
import com.yzhd_yl.service.ISysAdminService;
import com.yzhd_yl.util.PageList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@Api(tags = "管理员", description = "管理员操作接口", position = 3)
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private ISysAdminService sysAdminService;

    @org.springframework.beans.factory.annotation.Autowired(required=true)
    private BCryptPasswordEncoder passwordEncoder;

    @ApiOperation(value = "登陆", notes = "登陆", position = 27)
    @GetMapping(value = "/info")
    public ResResult<LoginAdmin> info() throws Exception {
        // 获取认证信息
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // 获取个人信息
        String adminname = authentication.getName();
        SysAdmin sysAdmin = sysAdminService.get(adminname);

        // 封装并返回结果
        LoginAdmin loginInfo = new LoginAdmin();
        loginInfo.setName(sysAdmin.getUsername());
        loginInfo.setAvatar(sysAdmin.getIcon());
        loginInfo.setNickName(sysAdmin.getNickName());
        //返回用户对应的菜单 根据userid查询
        List<SysMenu> menuList = sysAdminService.findByAdminId(sysAdmin.getId());
        loginInfo.setMenuList(menuList);

        return new ResResult<LoginAdmin>(ResResult.CodeStatus.OK, "获取用户信息", loginInfo);
    }


    /**
     * 获取所有用户的信息(分页)
     */
    @GetMapping(value = "/page")
    public ResResult page(AdminQuery userQuery) throws Exception {
        //返回用户对应的菜单 根据userid查询
        PageList pageList = sysAdminService.findPage(userQuery);
        return new ResResult (ResResult.CodeStatus.OK, "获取用户信息", pageList);
    }

    @PostMapping(value = "/saveAdmin")
    public ResResult saveAdmin(@RequestBody SysAdmin sysUser) throws Exception {
        try {
            sysAdminService.saveAdmin(sysUser);
            return  new ResResult<Map>(ResResult.CodeStatus.OK,
                    "保存成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new ResResult<Map>(ResResult.CodeStatus.FAIL,
                    "保存失败");
        }
    }

    @PutMapping(value = "/updateAdmin")
    public ResResult updateAdmin(@RequestBody SysAdmin sysUser) throws Exception {
        try {
            sysAdminService.updateAdmin(sysUser);
            return  new ResResult<Map>(ResResult.CodeStatus.OK,
                    "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new ResResult<Map>(ResResult.CodeStatus.FAIL,
                    "修改失败");
        }
    }

    @DeleteMapping(value = "/deleteAdmin/{id}")
    public ResResult deleteAdmin(@PathVariable("id") Long id) throws Exception {
        try {
            sysAdminService.deleteAdminById(id);
            return  new ResResult<Map>(ResResult.CodeStatus.OK,
                    "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new ResResult<Map>(ResResult.CodeStatus.FAIL,
                    "删除失败");
        }
    }

    //保存用户角色 saveUserRole


    @PostMapping(value = "/saveAdminRole")
    public ResResult saveAdminRole(@RequestBody List<Map> userRoleList) throws Exception {
        try {
            sysAdminService.saveAdminRole(userRoleList);
            return  new ResResult<Map>(ResResult.CodeStatus.OK,
                    "保存成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new ResResult<Map>(ResResult.CodeStatus.FAIL,
                    "保存失败");
        }
    }
}
