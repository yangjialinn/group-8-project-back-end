package com.yzhd_yl.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * url: /swagger-ui.html
 * 接口文档及测试页面
 * Created by xuyw on 2018/11/21.
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket buildDocket(){
        System.out.println("SwaggerConfig:------------------------------------> Swagger初始化成功");
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(buildApiInf())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yzhd_yl.controller"))
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo buildApiInf(){
        return new ApiInfoBuilder()
                .title("数据展示系统api说明文档")
                .description("springboot swagger2")
                .contact(new Contact("Allen","",""))
                .build();
    }
}
