package com.yzhd_yl.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @description: 配置图片访问路径
 */
@Configuration
public class MyPicConfig implements WebMvcConfigurer {

    @Value("${file.upload.spotsFmPath}")
    private String spotsFmUploadPath;


    @Value("${file.upload.headImgPath}")
    private String headImgUploadPath;

    @Value("${file.upload.path}")
    private String uploadNotesPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations(
                "classpath:/static/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations(
                "classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations(
                "classpath:/META-INF/resources/webjars/");

    }
}