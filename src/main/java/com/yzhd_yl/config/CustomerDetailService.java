package com.yzhd_yl.config;

import com.yzhd_yl.AdminSecurity;
import com.yzhd_yl.entity.SysAdmin;
import com.yzhd_yl.service.ISysAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;

/**
 * 需要自定义UserDetailsService实现spring security的UserDetailsService接口
 */
@Service
public class CustomerDetailService implements UserDetailsService {

    @Autowired
    private ISysAdminService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //根据用户名查询出用户
        SysAdmin user = userService.get(username);
        if(user != null) {
            //构建所有权限集合==ROLE_角色+权限
            HashSet<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();

            //返回更多信息的登录对象
            return new AdminSecurity(user,authorities);
        }else{
            return null;
        }
    }
}
